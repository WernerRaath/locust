import os

from yaml import load, dump
from jinja2 import Environment, FileSystemLoader

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

env = Environment(
    loader=FileSystemLoader("/opt/templates/")
)

config = load(open('/opt/config.yml'), Loader=Loader)

templates = env.list_templates()
print(f"Generating new versions of: {templates}")

for t in templates:
    template = env.get_template(t)
    content = template.render(config)
    with open(f"/opt/{t}", 'w+') as f:
        f.write(content)

print(f"Generated {templates}")
