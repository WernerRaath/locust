import random
import os

from locust import HttpLocust, TaskSet, task

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

config = load(open('/scripts/tests.yml'), Loader=Loader)

urls = []

for service in config:
    s = config[service]
    nginx_host = s.get('nginx_host', None)
    
    for url_conf in s["urls"]:
        for data in url_conf["data"]:
            formatted_url = url_conf["url"].format(**data).replace(" ", "%20")
            url = s['host'] + formatted_url
            urls.append(url)
            if nginx_host:
                url = nginx_host + formatted_url
                urls.append(url)
print("=" * 60)
print((" " * 20) + "TEST URLS")
print("=" * 60)
print("\n".join(urls))
print("=" * 60)

class APIActions(TaskSet):
    @task
    def test(self):
        for url in urls:
            self.client.get(url)        

class APIUser(HttpLocust):
    task_set = APIActions
    min_wait = 0
    max_wait = 0