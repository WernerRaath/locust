import argparse
import traceback
import json
import urllib.request
import sys

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def test_url(url):
    try:
        response = urllib.request.urlopen(url)
        status = response.status
        message = ""
    except Exception as err:
        try:
            try:
                e = err.read().decode('utf-8')
                error = json.loads(e)
                status = error["httpStatusCode"]
                message = error['error']
            except Exception as url_err:
                status = 400
                message = str(err)
        except Exception as err_1:
            message = err_1
            status = 400
    return status, message

def main(config):
    
    stats = ""
    for service in config:
        total = 0
        failed = 0
        s = config[service]
        nginx_host = s.get('nginx_host', None)
        for url_conf in s["urls"]:
            for data in url_conf["data"]:
                formatted_url = url_conf["url"].format(**data).replace(" ", "%20")
                url =  s["host"] + formatted_url
                status, message = test_url(url)
                if message:
                    message = f": {message}"
                print(service.upper().ljust(10), f"{status} = {formatted_url} {message}")
                if status != 200:
                    failed += 1
                total += 1
                if nginx_host is not None:
                    formatted_url = url_conf["url"].format(**data).replace(" ", "%20")
                    url = nginx_host + formatted_url
                    status, message = test_url(url)
                    if message:
                        message = f": {message}"
                    print((service.upper() + " NGINX").ljust(10), f"{status} = {formatted_url} {message}")
                    if status != 200:
                        failed += 1
                    total += 1

        stats += service.upper().ljust(10) + f" TOTAL: {total} | FAILED: {failed}\n"
    print(stats)
    if failed > 0:
        return 1
    else:
        return 0

if __name__ == "__main__":
    config = load(open('/scripts/tests.yml'), Loader=Loader)
    sys.exit(main(config))
    