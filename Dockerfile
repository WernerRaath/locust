FROM python:3.6.6-alpine3.8

COPY scripts/. /scripts/

RUN apk --no-cache add g++ postgresql-dev \ 
      && apk --no-cache add zeromq-dev \
      && pip install locustio pyzmq psycopg2 pyyaml jinja2

WORKDIR /scripts

CMD locust -f locustfile.py --host 0.0.0.0